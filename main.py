#if __name__ == '__main__'
#  print("hello world")

import pandas
from apyori import apriori
from sklearn import model_selection

def load_data(filename):
  data = pandas.read_csv(filename)
  return data
data = load_data('store_data.csv')
data2 = load_data('store2_data.csv')

def preprocess(data):
  item_list = data.unstack().dropna().unique()
  print(item_list)
  print("# items:",len(item_list))

data = pandas.read_csv('store_data.csv')
print(data)

item_list = data.unstack().dropna().unique()
print(item_list)
print("# items:",len(item_list))

train, test = model_selection.train_test_split(data, test_size=0.1)
print("train", train)
print("test", test)

train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
print("transform")
print(train)

for i in train[:10]:
    print(i)

def model(train_data, min_support=0.0045, min_confidence=0.2, min_left=3, min_length=2):
  result = list(apriori(train_data, min_support=min_support))
result = list(apriori(train))
print("result")
print(result)

for rule in result:
  print(rule[0])
  print("----------------------------")

result = list(apriori(train, min_support=0.0045, min_confidence=0.2, min_life=3, min_length=2))
print("result")
print(result)

for rule in result:
  pair = rule[0]
  components = (i for i in pair)
  print(pair)
  print('Rule:', components[0], '->', components[1])
  print('Support', rule[1])
  print('Confidence', rule[2][0][2])
  print('Lift', rule[2][0][3])
  print("============")


